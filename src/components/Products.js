import React, { Component } from "react";
import EditProductDetails from "./EditProductDetails";
import "./ProductsStyles.css"

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      myData: "",
      clonedData: "",
      productCards: "",
      individualCardDetails: "",
      restoreCardDetails: "",
      updateProductDetails : "",
      idOfIndividualCard: "",
      resetForm: false,
      isLoading: true,
      dataFetchingError: false
    };

    this.addProductCards = this.addProductCards.bind(this);
    this.selectProductCard = this.selectProductCard.bind(this);
    this.updateIndividualCard = this.updateIndividualCard.bind(this);
    this.changeWholeData = this.changeWholeData.bind(this);
    this.resetFormDetails = this.resetFormDetails.bind(this);
  }

  componentDidMount() {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => {
        this.setState(
          {
            myData: json,
            clonedData: json
          },
          () => {
            this.addProductCards(this.state.myData);
          }
        );
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          isLoading: false,
          dataFetchingError: true
        })
      });
  }


  addProductCards(data) {
    let cardsArray = [];

    cardsArray = data.map((object) => {
      return (
        <div className="col-12 col-md-3  mb-3" key={object.id}>
          <div className="card-container p-3 d-flex flex-column justify-content-end">
            <div className="product-image-container mb-3">
              <img className="product-image" src={object.image} alt="" />
              </div>
            {/* Product heading and price container */}
            <div className="mb-2">
              <h6>{object.title}</h6>
            </div>

            {/* Rating and pricing container */}
            <div className="d-flex justify-content-between mb-2">
              <div>
                <p>{`Rating: ${object.rating.rate}`}</p>
              </div>
              <div className="d-flex mb-2">
                <i className="fa-solid fa-indian-rupee-sign rupee-icon"></i>
                <p>{object.price}</p>
              </div>
            </div>
            <div className="mb-2">
              <p>{`Category: ${object.category}`}</p>
            </div>
            <div className="mb-2">
              <button className="btn btn-primary" id={object.id} onClick={this.selectProductCard}>
                Edit Product Details
              </button>
            </div>
            
          </div>
        </div>
      );
    });

    this.setState({
      isLoading: false,
      productCards: cardsArray,
    });

    return null;
  }

  updateIndividualCard(productObject) {
    let key = (Object.keys(productObject[0]))[0];
    let newObject;
    let stateObject = {}

    if(key !== "rating") {
      this.setState((prevObject) => {
        newObject = prevObject.individualCardDetails[0];
        newObject[key] = productObject[0][key]
        // console.log(productObject[0][key]);
        return stateObject['individualCardDetails'] = newObject;
      }) 
    } 
    else {
      this.setState((prevObject) => {
        newObject = {...prevObject.individualCardDetails[0]};
        newObject[key]["rate"] = productObject[0][key]
        // console.log(productObject[0][key]);
        return stateObject['individualCardDetails'] = newObject;
      }) 
      }
    }
  

  selectProductCard(event) {
    let requiredCardDetails = [];
    requiredCardDetails = this.state.myData.filter((productObject) => {
        return Number(event.target.id) === productObject.id;
    })

    this.setState({
        idOfIndividualCard : event.target.id,
        individualCardDetails: requiredCardDetails,
    })
  }

  changeWholeData(data) {
    this.addProductCards(data);
    this.setState({
      individualCardDetails: ""
    })
  }


  resetFormDetails(id) {
    window.location.reload(true)
    // let requiredCardDetails = [];
    // requiredCardDetails = this.state.clonedData.filter((productObject) => {
    //     return Number(id) === productObject.id;
    // })
    // console.log(this.state.clonedData);
    // this.setState({
    //     idOfIndividualCard : id,
    //     individualCardDetails: requiredCardDetails,
    // })
  }

  render() {

    if(this.state.dataFetchingError) {
      return (
        <>
          <div className="api-unsuccessful">
            <h1>404 Not Found</h1>
            <h4>
              The Page you are requesting is not found. Please try again after
              some time.
            </h4>
          </div>
        </>
      );
    }

    if(this.state.isLoading) {
      return(
        <>
        <div className="spinner-container">
          <div className="spinner-container">
            <div className="text-center">
              <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
              </div>
            </div>
          </div>
        </div>
        </>
      )
    }
  

    if (this.state.productCards && this.state.individualCardDetails) {
        return(
            <EditProductDetails resetFormDetails={this.resetFormDetails} totalData={this.state.myData}  selectedCardDetails={this.state.individualCardDetails} changeWholeData={this.changeWholeData} updateIndividualCard={this.updateIndividualCard} idOfIndividualCard={this.state.idOfIndividualCard}/>
        )
      
    } else if(this.state.productCards) {
        return (
            <>
              <div className="container p-3">
                <div className="row ">
                    {this.state.productCards}
                </div>
              </div>
            </>
          );
    }
  }
}

export default Products;
