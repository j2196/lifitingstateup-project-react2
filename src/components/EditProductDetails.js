import React, { Component } from "react";
import "./ProductsStyles.css";

class EditProductDetails extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.resetFormDetails = this.resetFormDetails.bind(this);
    this.goToMainPage = this.goToMainPage.bind(this);
  }

  // getUpdatedValues(event) {
  //   event.preventDefault();

  //   let keysObject = Object.keys(this.props.selectedCardDetails[0]);
  //   let updatedCardObject = {}
  //   keysObject.map((object, index) => {
  //     if (event.target[index].value && object !== "rating") {
  //       updatedCardObject[object] = event.target[index].value;
  //     } else if(object === "rating")  {
  //       updatedCardObject[object] = {rate: event.target[index].value};
  //     }
  //     return object;
  //   });

  //   this.props.updateIndividualCard([updatedCardObject]);
  // }

  goToMainPage() {
    let requiredTotalData = [];

    requiredTotalData = this.props.totalData.map((object) => {
      if (object.id === Number(this.props.idOfIndividualCard)) {
        return this.props.selectedCardDetails[0];
      } else {
        return object;
      }
    });
    this.props.changeWholeData(requiredTotalData);
  }

  handleChange(event) {
    let keysObject = Object.keys(this.props.selectedCardDetails[0]);
    let key = keysObject[event.target.id];
    this.props.updateIndividualCard([{[key]: event.target.value}]);
  }

  resetFormDetails() {
    this.props.resetFormDetails(this.props.idOfIndividualCard)
  }

  render() {
    let object = this.props.selectedCardDetails[0];
    let keysObject = Object.keys(this.props.selectedCardDetails[0]);

    let updateForm = [];

    updateForm = keysObject.map((productDetails, index) => {

      if(productDetails !== "rating" && productDetails !== "id") {
        return (
          <div key={index} className="mb-3">
            <label htmlFor={index}>{productDetails}</label> <br />
            <input
              className="border-primary input-element"
              defaultValue={object[productDetails]}
              id={index}
              type="text"
              onChange={this.handleChange}
            />
          </div>
        );
      } else if(productDetails === "id") {
        <div key={index} className="mb-3">
          <label htmlFor={index}>{productDetails}</label> <br />
          <input
            className="border-primary input-element"
            defaultValue={object[productDetails]}
            id={index}
            type="text"
            onChange={this.handleChange}
            readOnly
          />
      </div>
      }
      
      else {
        return (
          <div key={index} className="mb-3">
            <label htmlFor={index}>{productDetails}</label> <br />
            <input
              className="border-primary input-element"
              defaultValue={object[productDetails].rate}
              id={index}
              type="text"
              onChange={this.handleChange}
            />
          </div>
        )
      }
      
      return null
    });

    return (
      <>
        <div className="container">
          <div className="row p-3 ">
            <div className="col-6  mb-3">
              <div className="d-flex flex-column">
                <div className="product-image-individual-container mb-3">
                  <img
                    className="product-image-individual"
                    src={object.image}
                    alt=""
                  />
                </div>
                {/* Product heading and price container */}
                <div className="mb-2">
                  <h6>{object.title}</h6>
                </div>

                {/* Rating and pricing container */}
                <div className="d-flex justify-content-between items-alignment-individual">
                  <div>
                    <p>{`Rating: ${object.rating.rate}`}</p>
                  </div>
                  <div className="d-flex mb-2">
                    <i className="fa-solid fa-indian-rupee-sign rupee-icon"></i>
                    <p>{object.price}</p>
                  </div>
                </div>
                <div>
                  <p>{object.description}</p>
                </div>
                <div>
                  <p>{`Category: ${object.category}`}</p>
                </div>
              </div>
            </div>

            <div className="col-6">
              <form
                onSubmit={this.getUpdatedValues}
                noValidate="noValidate"
                className="text-center"
              >
                <h3>EDIT DETAILS</h3>
                {updateForm}
                {/* <button className="m-3 btn btn-primary" type="submit">
                  Save Changes
                </button> */}
                <button
                  className="m-3 btn btn-primary"
                  type="button"
                  onClick={this.goToMainPage}
                >
                  Go To Products
                </button>
                <button className="m-3 btn btn-primary" type="button" onClick={this.resetFormDetails}>
                  Reset Details
                </button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default EditProductDetails;
